<?php
namespace App\DataPersister;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Mission;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\Security\Core\Security;

class MissionDataPersister implements DataPersisterInterface
{
    private $entityManager;
    private $JWTEncoder;
    private $security;
    public function __construct(EntityManagerInterface $entityManager, JWTEncoderInterface $JWTEncoder, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->JWTEncoder = $JWTEncoder;
        $this->security = $security;
    }
    public function supports($data): bool
    {
        return $data instanceof Mission;
    }
    /**
     * @param Mission $data
     */
    public function persist($data)
    {
        $data->setIdAuthor($this->JWTEncoder->decode($this->security->getToken()->getCredentials())["id"]);
        $data->setIdStatus(1);
        $data->setPublicationDate(new \DateTime('NOW'));
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }
    public function remove($data)
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
