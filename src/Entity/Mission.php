<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MissionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      collectionOperations={
 *          "GET" = {
 *              "security"="is_granted('ROLE_USER')"
 *          },
 *          "POST" = {
 *              "security"="is_granted('ROLE_USER')"
 *          }
 *     },
 *     itemOperations={
 *     "GET" = {
 *              "security"="is_granted('ROLE_USER')"
 *          },
 *     "PUT" = {
 *              "security"="is_granted('MISSION_EDIT',object)"
 *          },
 *     "DELETE" = {
 *              "security"="is_granted('MISSION_DELETE',object)"
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass=MissionRepository::class)
 */
class Mission
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $idAuthor;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publicationDate;

    /**
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $remuneration;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idCandidate;

    /**
     * @ORM\Column(type="integer")
     */
    private $idStatus;

    /**
     * @ORM\OneToMany(targetEntity=Positioning::class, mappedBy="idMission")
     */
    private $positionings;

    public function __construct()
    {
        $this->positionings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIdAuthor(): ?int
    {
        return $this->idAuthor;
    }

    public function setIdAuthor(int $idAuthor): self
    {
        $this->idAuthor = $idAuthor;

        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(\DateTimeInterface $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getRemuneration(): ?float
    {
        return $this->remuneration;
    }

    public function setRemuneration(?float $remuneration): self
    {
        $this->remuneration = $remuneration;

        return $this;
    }

    public function getIdCandidate(): ?int
    {
        return $this->idCandidate;
    }

    public function setIdCandidate(?int $idCandidate): self
    {
        $this->idCandidate = $idCandidate;

        return $this;
    }

    public function getIdStatus(): ?int
    {
        return $this->idStatus;
    }

    public function setIdStatus(int $idStatus): self
    {
        $this->idStatus = $idStatus;

        return $this;
    }

    /**
     * @return Collection|Positioning[]
     */
    public function getPositionings(): Collection
    {
        return $this->positionings;
    }

    public function addPositioning(Positioning $positioning): self
    {
        if (!$this->positionings->contains($positioning)) {
            $this->positionings[] = $positioning;
            $positioning->setIdMission($this);
        }

        return $this;
    }

    public function removePositioning(Positioning $positioning): self
    {
        if ($this->positionings->contains($positioning)) {
            $this->positionings->removeElement($positioning);
            // set the owning side to null (unless already changed)
            if ($positioning->getIdMission() === $this) {
                $positioning->setIdMission(null);
            }
        }

        return $this;
    }
}
