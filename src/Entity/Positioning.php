<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PositioningRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PositioningRepository::class)
 */
class Positioning
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $idCanditate;

    /**
     * @ORM\ManyToOne(targetEntity=Mission::class, inversedBy="positionings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idMission;

    /**
     * @ORM\Column(type="datetime")
     */
    private $positioningDate;

    /**
     * @ORM\Column(type="text")
     */
    private $candidacy;

    /**
     * @ORM\Column(type="integer")
     */
    private $idStatus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdCanditate(): ?int
    {
        return $this->idCanditate;
    }

    public function setIdCanditate(int $idCanditate): self
    {
        $this->idCanditate = $idCanditate;

        return $this;
    }

    public function getIdMission(): ?Mission
    {
        return $this->idMission;
    }

    public function setIdMission(?Mission $idMission): self
    {
        $this->idMission = $idMission;

        return $this;
    }

    public function getPositioningDate(): ?\DateTimeInterface
    {
        return $this->positioningDate;
    }

    public function setPositioningDate(\DateTimeInterface $positioningDate): self
    {
        $this->positioningDate = $positioningDate;

        return $this;
    }

    public function getCandidacy(): ?string
    {
        return $this->candidacy;
    }

    public function setCandidacy(string $candidacy): self
    {
        $this->candidacy = $candidacy;

        return $this;
    }

    public function getIdStatus(): ?int
    {
        return $this->idStatus;
    }

    public function setIdStatus(int $idStatus): self
    {
        $this->idStatus = $idStatus;

        return $this;
    }
}
